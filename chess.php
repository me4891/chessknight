<?php
class Chess {
	private $chessBoard=array();
	private $letters=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','Y','Z');
	private $lettersToHide=array();
	private $startPositions=array('0,1','0,6','7,1','7,6');

	private $jArr=array('A','B','C','D','E','F','G','H');
	private $iArr=array('8','7','6','5','4','3','2','1');

	function __construct() {
		//create board
		for($i=0;$i<8;$i++) {
			for($j=0;$j<8;$j++) {
				$this->chessBoard[$i][$j]='.';
			}
		}
	}

	private function drawChessBoard() {
		//echo 'draw';
		for($i=0;$i<8;$i++) {
			echo '<div>';
			for($j=0;$j<8;$j++) {
				//if($j==0) echo '<div style="border:1px solid lightgray; width:20px; height:40px; line-height:40px; display:inline-block; text-align:center; background:darkgray;">'.$i.'</div>';
				if($j==0) echo '<div style="border:1px solid lightgray; width:20px; height:40px; line-height:40px; display:inline-block; text-align:center; background:black; color:white;">'.$this->iArr[$i].'</div>';
				$bgcolor='';
				if(($i+$j)%2==1) $bgcolor='background:lightgray;';
				echo '<div style="border:1px solid lightgray; width:40px; height:40px; line-height:40px; display:inline-block; text-align:center; '.$bgcolor.'">'.$this->chessBoard[$i][$j].'</div>';
			}
			echo '</div>';
		}

			echo '<div>';
			echo '<div style="border:1px solid lightgray; width:20px; height:20px; line-height:20px; display:inline-block; text-align:center; background:black; color:black;">.</div>';
			for($j=0;$j<8;$j++) {
				//echo '<div style="border:1px solid lightgray; width:40px; height:20px; line-height:20px; display:inline-block; text-align:center; background:darkgray;">'.$j.'</div>';
				echo '<div style="border:1px solid lightgray; width:40px; height:20px; line-height:20px; display:inline-block; text-align:center; background:black; color:white;">'.$this->jArr[$j].'</div>';
			}
			echo '</div>';


	}

	private function findNextMove($x,$y) {
		$moves=array('2,1','2,-1','-2,1','-2,-1','1,2','-1,2','1,-2','-1,-2');
		foreach($moves as $k=>$v) {
			$offset=explode(',',$v);
			$nextX=$x+$offset[0];
			$nextY=$y+$offset[1];
			if($nextX<8 OR $nextY<8 OR $nextX>=0 OR $nextY>=0) {
				if($this->chessBoard[$nextX][$nextY]=='.') {
					$possibleNextMove[]=$nextX.','.$nextY;
				}
			}

		}
		//print_r($possibleNextMove);
		return($possibleNextMove);
	}

	private function fillGaps() {
		for($i=0;$i<8;$i++) {
			for($j=0;$j<8;$j++) {
				if($this->chessBoard[$i][$j]=='.') $this->chessBoard[$i][$j]=$this->letters[array_rand($this->letters)];
			}
		}
	}

	public function encode($text) {
		//echo 'word to hide: '.$text.'<hr>';
		$lettersToHide=str_split(($text));

		foreach($lettersToHide as $letterToHide) {
			echo $letterToHide;
			if(!isset($this->lastPosition)) {
				$offset=explode(',',$this->startPositions[array_rand($this->startPositions)]);
			}
			else {
				$offset=explode(',',$this->lastPosition[array_rand($this->lastPosition)]);
			}
			$this->chessBoard[$offset[0]][$offset[1]]=$letterToHide;
			//$this->drawChessBoard();
			$this->lastPosition=$this->findNextMove($offset[0],$offset[1]);
		}

		$this->fillGaps();
		//echo 'final';
		$this->drawChessBoard();

	}
}

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('PAJDA');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('ROWER');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('KOREK');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('KROWA');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('SCENA');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('WATAHA');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('DAKTYL');
echo '</div>';

echo '<div style="display:inline-block; margin:2em;">';
$code=new Chess;
$code->encode('MAKOWIEC');
echo '</div>';


?>
